#Основной задачей данного микросервиса будет получение и отображение курсов валют с сайта НБ РБ.
________
 

- [X] Требуется создать 2 точки входа (endpoint) - формат данных, метод запросов, формат ответа и набор дополнительных данных запроса/ответа определяется разработчиком:

 

- [X] 1. Endpoint с входящим параметром - дата.

Результат запроса - ответ, указывающий на корректность выполнения загрузки данных в разработанную систему о курсах за выбранную дату

API национального банка: https://www.nbrb.by/apihelp/exrates

Способ хранения информации (кэш, БД или иное - определяется разработчиком)

 

- [X] 2. Endpoint с входящими параметрами - дата и код валюты

Результат запроса - информация о курсе валюты за указанный день

 

- [X] Ответ на каждый запрос требуется подписать добавлением CRC32 тела ответа в заголовок

 

Что хотелось бы видеть дополнительно:

- [X] - логирование запросов и ответов

- [X] - в запросе 2 - информацию об изменении курса по сравнению с предыдущим рабочим днем (вырос, уменьшился)

- [X] - описание разработанного протокола взаимодействия

- [X] - иструкцию по установке

## Установка
1. Склонировать репозиторий с Gitlub
https://gitlab.com/Pulemet911/currency_converter.git/n
2. Перейти в директорию проекта
```
pip install -r requirements.txt
```
## Запуск приложения
### Применить миграции
```
python manage.py migrate
```
### Запуск сервера
```
python manage.py runserver
```

## Установка проекта с помощью docker-compose
## Склонировать репозиторий с Gitlub
1. Склонировать репозиторий с Gitlub
https://gitlab.com/Pulemet911/currency_converter.git/n
2. Перейти в директорию проекта
```
docker-compose up -d --build
```
```
docker-compose down -v
```
```
docker-compose up -d
```

##Остановка работы контейнеров 
```
docker-compose stop
```

## Главная страница
http://localhost:8000/
## Страница загрузки данных
http://localhost:8000/upload/
## Страница просмотра курса валюты
http://localhost:8000/rate/