import zlib
from datetime import timedelta
import time
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import View
import requests
from exchange_rates.forms import DateForm, DateCodeForm
from exchange_rates.models import Currency
import logging

logger = logging.getLogger(__name__)


class MainView(View):

    def get(self, request):
        date1 = time.strftime("%Y-%m-%dT%H:%M:%S")
        url = f'https://www.nbrb.by/api/exrates/rates?ondate={date1}&periodicity=0'
        resp = requests.get(url)
        api = resp.json()
        found = Currency.objects.filter(date=str(date1)[0:10] + 'T00:00:00')
        if not found:
            create_db(api)
        crc32 = zlib.crc32(bytes(str(request.headers), 'utf-8'))
        context = {
            'crc32': crc32
        }
        logger.info('Вы перешли на главную страницу')
        return render(request, 'main.html', context=context)


def rate(request):
    if request.method == 'POST':
        form = DateCodeForm(request.POST)
        if form.is_valid():
            date = form.cleaned_data['date']
            cur_id = form.cleaned_data['cur_id']
            found = Currency.objects.filter(date=str(date)[0:10] + 'T00:00:00', cur_id=cur_id)
            if not found:
                logger.info('Данные не загружены')
                return HttpResponse('Данные не загружены')
            date_last_day = date - timedelta(days=1)
            found_last_day = Currency.objects.filter(date=str(date_last_day)[0:10] + 'T00:00:00', cur_id=cur_id)

            if not found_last_day:
                url = f'https://www.nbrb.by/api/exrates/rates?ondate={str(date_last_day)[0:10]}&periodicity=0'
                resp = requests.get(url)
                api = resp.json()
                create_db(api)
            cur1 = Currency.objects.filter(date=str(date)[0:10] + 'T00:00:00', cur_id=cur_id).first()
            cur2 = Currency.objects.filter(date=str(date_last_day)[0:10] + 'T00:00:00', cur_id=cur_id).first()
            dif = round(cur1.cur_official_rate - cur2.cur_official_rate, 3)
            crc32 = zlib.crc32(bytes(str(request.headers), 'utf-8'))
            context = {
                'found': found,
                'dif': dif,
                'crc32': crc32
            }
            logger.info(f'Просмотр курса {found} - {cur_id}')
            return render(request, 'exchange_rates/viewing_the_course.html', context=context)
    else:
        form = DateCodeForm()
        date1 = time.strftime("%Y-%m-%d")
        currency = Currency.objects.filter(date=date1 + 'T00:00:00')
        crc32 = zlib.crc32(bytes(str(request.headers), 'utf-8'))
        context = {
            'form': form,
            'currency': currency,
            'crc32': crc32
        }
        logger.info('Вы перешли на страницу просмотра курса валюты')
    return render(request, 'exchange_rates/rate.html', context=context)


def upload(request):
    if request.method == 'POST':
        form = DateForm(request.POST)
        if form.is_valid():
            date = str(form.cleaned_data['date'])[0:10]
            url = f'https://www.nbrb.by/api/exrates/rates?ondate={date}&periodicity=0'
            resp = requests.get(url)
            crc32 = zlib.crc32(bytes(str(request.headers), 'utf-8'))
            api = resp.json()
            if api:
                found = Currency.objects.filter(date=date + 'T00:00:00')
                if not found:
                    create_db(api)
                    logger.info('Данные успешно загружены')
                return render(request, 'exchange_rates/update_ok.html', {'crc32': crc32})
            else:
                logger.info('Даты не существует')
                return HttpResponse('Даты не существует')
    else:
        form = DateForm
        crc32 = zlib.crc32(bytes(str(request.headers), 'utf-8'))
    logger.info('Вы перешли на страницу загрузки курса валюты')
    return render(request, 'exchange_rates/update_rates.html', {'form': form, 'crc32': crc32})


def create_db(api):
    for i in api:
        Currency.objects.create(
            cur_id=i['Cur_ID'],
            date=i['Date'],
            cur_abbreviation=i['Cur_Abbreviation'],
            cur_scale=i['Cur_Scale'],
            cur_name=i['Cur_Name'],
            cur_official_rate=i['Cur_OfficialRate']
        )
