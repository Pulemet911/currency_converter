from django.db import models


class Currency(models.Model):
    cur_id = models.IntegerField(verbose_name='код валюты')
    date = models.CharField(max_length=30, verbose_name='дата курса валюты')
    cur_abbreviation = models.CharField(max_length=3, verbose_name='аббревиатура валюты')
    cur_scale = models.IntegerField(verbose_name='количество')
    cur_name = models.CharField(max_length=50, verbose_name='название валюты')
    cur_official_rate = models.FloatField(verbose_name='курс валюты')

    def __str__(self):
        return self.cur_name

    class Meta:
        verbose_name = 'валюта'
        db_table = 'rates'
        ordering = ['date']