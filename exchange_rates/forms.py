from django import forms


class DateForm(forms.Form):
    date = forms.DateTimeField(
        input_formats=['%Y-%m-%d']
    )


class DateCodeForm(forms.Form):
    date = forms.DateTimeField(
        input_formats=['%Y-%m-%d']
    )
    cur_id = forms.IntegerField(min_value=400, max_value=500)
