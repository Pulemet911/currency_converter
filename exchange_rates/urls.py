from django.urls import path
from exchange_rates.views import MainView, upload, rate

urlpatterns = [
    path('', MainView.as_view(), name='main'),
    path('upload/', upload, name='upload'),
    path('rate/', rate, name='rate'),
    # path('qqq/', MainingView.as_view(), name='qqq'),
    # path('upload/', upload_rate, name='upload')
]