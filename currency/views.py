from django.contrib.sites import requests
from django.shortcuts import render, redirect
from django.views import View


class MainView(View):

    def get(self, request):
        url = 'https://www.nbrb.by/api/exrates/rates?ondate=2023-2-3&periodicity=0'
        return redirect(url)


def upload(request):
    return None